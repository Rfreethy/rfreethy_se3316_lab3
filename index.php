<!DOCTYPE html>
<html lang="en">
  <head>
   
  <meta http-equiv="Content-Type" content="text/html;charset=us-ansi">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Lab3 SE3316A</title>
    
  <link href='https://fonts.googleapis.com/css?family=Cuprum|Cookie' rel='stylesheet' type='text/css'>  
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.css" rel="stylesheet">


  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  <!-- Custom CSS import -->
  <link href="custom.css" rel="stylesheet" type='text/css'> 
  
  </head>
  
  
  
  <body>
      
    <div id="header">
      <div id="lab3-logo">
        <a href="/">LAB 3</a>
      </div>
      <div id="nav-bar">
        <ul>
          <a href="index.php"><li> Home </li></a>
          <a href="about.php"><li> About </li></a>
          <a href="work.php"><li> Work </li></a>
          <a href="artists.php"><li> Artists </li></a>
        </ul> 
      </div>
    </div>
  
  
  
    <div id="home-carousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#home-carousel" data-slide-to="0" class="active"></li>
        <li data-target="#home-carousel" data-slide-to="1"></li>
        <li data-target="#home-carousel" data-slide-to="2"></li>
        <li data-target="#home-carousel" data-slide-to="3"></li>
        <li data-target="#home-carousel" data-slide-to="4"></li>
      </ol>
        
      <div class="carousel-inner">
<?php
$paintingsFile = file("data-files/paintings.txt");
    
$paintings = array();
    
foreach($paintingsFile as $data) {
    $info = explode("~", $data);
    //$info is now an array of data
    $paintings[] = $info; //therefore by putting $info into $paintings it makes $paintings a double array 
}
for ($i = 0; $i < 5; $i++){
    echo "<div class='item'><img src='resources/paintings/medium/". $paintings[$i][3] . ".jpg' alt='". $paintings[$i][4] ."' title='".$paintings[$i][4]."' rel='#PaintingThumb' /><div class='container'><div class='carousel-caption'><h1>". $paintings[$i][4] ."</h1><p>". $paintings[$i][6] ."</p><p><a class='btn btn-lg btn-primary' href='". $paintings[$i][12] ."' target='_blank' role='button'>Learn more</a></p></div></div></div>";
}
?>
      </div>
      <a class="left carousel-control" href="#home-carousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#home-carousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
    
       <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">

<?php
for ($i = 5; $i < 8; $i++){
  echo  '<div class="col-lg-4"><img class="img-circle" src="resources/paintings/medium/'. $paintings[$i][3] .'.jpg" alt="'. $paintings[$i][4] .'" title="'. $paintings[$i][4] .'" style="width:100px; height:100px;" /><h2>'. $paintings[$i][4] . '</h2><p class="text-justify">'. explode(".", $paintings[$i][5])[0] . '</p><p><a class="btn btn-default" target="_blank" href="'. $paintings[$i][12] . '" role="button">View details &raquo;</a></p></div><!-- /.col-lg-4 -->';
}
?>

    </div>
    <div class="row">

<?php
for ($i = 8; $i < 11; $i++){
  echo  '<div class="col-lg-4"><img class="img-circle" src="resources/paintings/medium/'. $paintings[$i][3] .'.jpg" alt="'. $paintings[$i][4] .'" title="'. $paintings[$i][4] .'" style="width:100px; height:100px;" /><h2>'. $paintings[$i][4] . '</h2><p class="text-justify">'. explode(".", $paintings[$i][5])[0] . '</p><p><a class="btn btn-default" target="_blank" href="'. $paintings[$i][12] . '" role="button">View details &raquo;</a></p></div><!-- /.col-lg-4 -->';
}
?>

    </div>
  
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
      
    <!-- This javascript will add the active class to the first item in the carousel -->
    <script>
    
    $(".item:first-child").addClass("active");
    
    </script>
    
  </body>
</html>