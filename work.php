<!DOCTYPE html>
<html lang="en">
  <head>
   <meta http-equiv="Content-Type" content="text/html;charset=us-ansi">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <title>Lab3 SE3316A</title>
    
<link href='https://fonts.googleapis.com/css?family=Cuprum|Cookie' rel='stylesheet' type='text/css'>  

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="custom.css" rel="stylesheet" type='text/css'>
  </head>
  
   <body>
  <header>

   <div id="topHeaderRow">
      <div class="container">
         <nav role="navigation" class="navbar navbar-inverse ">
            <div class="navbar-header">
               <button data-target=".navbar-ex1-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
               <p class="navbar-text">Welcome to <strong>Art Store</strong>, <a class="navbar-link" href="#">Login</a> or <a class="navbar-link" href="#">Create new account</a></p>
            </div>

            <div class="collapse navbar-collapse navbar-ex1-collapse pull-right">
               <ul class="nav navbar-nav">
                  <li><a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a></li>
                  <li><a href="#"><span class="glyphicon glyphicon-gift"></span> Wish List</a></li>
                  <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</a></li>
                  <li><a href="#"><span class="glyphicon glyphicon-arrow-right"></span> Checkout</a></li>                  
               </ul>
            </div>  <!-- end .collpase --> 
         </nav>  <!-- end .navbar --> 
      </div>  <!-- end .container --> 
   </div>  <!-- end #topHeaderRow --> 
   
   <div id="logoRow">
      <div class="container">
         <div class="row">
            <div class="col-md-8">
                <h1>Art Store</h1> 
            </div>
            
            <div class="col-md-4">
               <form role="search" class="form-inline">
                  <div class="input-group">
                     <label for="search" class="sr-only">Search</label>
                     <input type="text" name="search" placeholder="Search" class="form-control">
                     <span class="input-group-btn">
                     <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                     </span>
                  </div>
               </form> 
            </div>   <!-- end .navbar --> 
         </div>   <!-- end .row -->        
      </div>  <!-- end .container --> 
   </div>  <!-- end #logoRow --> 
   
   <div id="mainNavigationRow">
      <div class="container">

         <nav role="navigation" class="navbar navbar-default">
            <div class="navbar-header">
               <button data-target=".navbar-ex1-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
            </div>

            <div class="collapse navbar-collapse navbar-ex1-collapse">
             <ul class="nav navbar-nav">
               <li><a href="index.php">Home</a></li>
               <li><a href="about.php">About Us</a></li>
               <li class="active"><a href="#">Art Works</a></li>
               <li><a href="artists.php">Artists</a></li>
               <li class="dropdown">
                 <a data-toggle="dropdown" class="dropdown-toggle" href="#">Specials <b class="caret"></b></a>
                 <ul class="dropdown-menu">
                   <li><a href="#">Special 1</a></li>
                   <li><a href="#">Special 2</a></li>                   
                 </ul>
               </li>
             </ul>              
            </div>
         </nav>  <!-- end .navbar --> 
      </div>  <!-- end container -->
    </div>  <!-- end mainNavigationRow -->
   
    </header>
  
    <div class="container">
        <div class="row">
            <div id="workCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
<?php
$paintingsFile = file("data-files/paintings.txt");
    
$paintings = array();
    
foreach($paintingsFile as $data) {
    $info = explode("~", $data);
    //$info is now an array of data
    $paintings[] = $info; //therefore by putting $info into $paintings it makes $artists a double array 
}
for ($i = 0; $i < sizeof($paintings); $i++){
    echo "
<div class='item'>
    <div class='container'>
        <div class='col-md-10'>
            <h2>". $paintings[$i][4] ."</h2>
            <p><a href='#'>". $paintings[$i][6] ."</a></p>
            <div class='row'>
                <div class='col-md-5'><img class='img-thumbnail img-responsive' src='resources/paintings/medium/". $paintings[$i][3] .".jpg' alt='". $paintings[$i][4] ."' title='". $paintings[$i][4] ."'/></div>
                <div class='col-md-7'>
                    <p style='text-align: justify; 	text-justify: inter-word;'>". $paintings[$i][5] ."</p>
                    <p class='price'>". $paintings[$i][11] ."</p>
                    <div class='btn-group btn-group-lg'>
                        <button class='btn btn-default' type='button'>
                            <a href='#'><span class='glyphicon glyphicon-gift'></span> Add to Wish List</a>  
                        </button>
                        <button class='btn btn-default' type='button'>
                            <a href='#'><span class='glyphicon glyphicon-shopping-cart'></span> Add to Shopping Cart</a>
                        </button>
                    </div>               
                    <p>&nbsp;</p>
                    <div class='panel panel-default'>
                        <div class='panel-heading'>Product Details</div>
                        <table class='table'>
                            <tbody><tr>
                                <th>Date:</th>
                                <td>". $paintings[$i][6] ."</td>
                            </tr>
                            <tr>
                                <th>Medium:</th>
                                <td>". $paintings[$i][9] ."</td>
                            </tr>  
                            <tr>
                                <th>Dimensions:</th>
                                <td>". $paintings[$i][7] ." x ". $paintings[$i][8] ."</td>
                            </tr> 
                            <tr>
                                <th>Home:</th>
                                <td><a href='#'>". $paintings[$i][10] ."</a></td>
                            </tr>  
                            <tr>
                                <th>Link:</th>
                                <td><a href='". $paintings[$i][12] ."' target='_blank'>Wiki</a></td>
                            </tr>     
                            </tbody>
                        </table>
                    </div>                              
                </div> 
            </div>  
        </div>
    </div>
</div>
    ";
}
?>
                </div>
                <a class="left carousel-control" href="#workCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                <a class="right carousel-control" href="#workCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
            </div><!-- end of carousel -->
        </div>
    </div>
      <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    
    
    <!-- This javascript will add the active class to the first item in the carousel -->
    <script>
    
    $(".item:first-child").addClass("active");
    
    </script>

</div>
                    